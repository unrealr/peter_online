(function($) {
    $.fn.blink = function(options) {
        var defaults = {
            delay: 500
        };
        var options = $.extend(defaults, options);

        return this.each(function() {
            var obj = $(this);
            setInterval(function() {
                if ($(obj).css("visibility") == "visible") {
                    $(obj).css('visibility', 'hidden');
                }
                else {
                    $(obj).css('visibility', 'visible');
                }
            }, options.delay);
        });
    }
}(jQuery)) ;

/////////////////////////////////////////////
///////////////////////////////////////////// 
/////////////////////////////////////////////

$(document).ready(function() {
    $('.arrowd').blink(); // default is 500ms blink interval.
    $('.blink_second').blink({
        delay: 100
    }); // causes a 100ms blink interval.
    $('.blink_third').blink({
        delay: 1500
    }); // causes a 1500ms blink interval.     
});

/////////////////////////////////////////////
///////////////////////////////////////////// 
///////////////////////////////////////////// 
    
  
        
        $("a[href='#sap']").click(function() {
  $("html, body").animate({scrollTop: $("#sap").offset().top});
  return false;
});