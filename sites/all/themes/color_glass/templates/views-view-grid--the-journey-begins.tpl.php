<?php
/**
 * @file
 * Views-view-table.tpl.php.
 */

/**
 * Template to display a view as a table.
 *
 * Fields Available.
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 *
 * @ingroup views_templates
 */
?>


   <?php $sandle=0; if (!empty($title) || !empty($caption)) : ?>
     <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>


    <?php foreach ($header as $field => $label): ?>

        <?php print $label; ?>

    <?php endforeach; ?>


  <?php endif; ?>
<div class="container-fluid content-boxes " style="    background-color: #f5f5f5;
    border-radius: 1%;">
    <div class="row content-boxes">
    <?php foreach ($rows as $row_count => $row): ?>

        <?php foreach ($row as $field => $content): ?>
            <div id="balogna" class="col-sm-12 col-md-6 col-lg-4 content-box" style="<?php if ($sandle==0): ?> background-color:navajowhite;
            <?php else: ?> background-color:white; <?php endif; ?> padding-right:3px;padding-top:3px;min-height:450px;">
                <?php print $content; ?>
            </div>
            <?php if ($sandle==0): $sandle++; else: $sandle=0; endif;?>
        <?php endforeach; ?>

    <?php endforeach; ?>
    </div>
</div>

